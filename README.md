# pg-manager

This project produces a container image based on Alpine that includes tooling for interacting with a PostgreSQL database. Packages included are the pg client binaries and aws-cli (for pulling/pushing backups).

## Scripts

1. `scripts/dump.sh` - Use `pg_dump` to create a compressed file. Upload to s3 bucket.
1. `scripts/restore.sh` - Use `pg_restore` to load/restore a compressed file. Downloaded from an s3 bucket.

## Environment Variables

The following are required for all files in `scripts/`:

| Variable | Description |
| --------- | ----------- |
| `AWS_ACCESS_KEY_ID` | The aws/minio access key id |
| `AWS_SECRET_ACCESS_KEY` | The aws/minio secret access key |
| `BACKUP_FILE` | The full path to the backup file being created/restored. Example: `/tmp/yourapp.dump` |
| `BUCKET_KEY` | The aws/minio bucket key. Example: `yourapp.dump` |
| `BUCKET` | The aws/minio bucket. Example: `backups` |
| `ENDPOINT_URL` | The aws/minio endpoint url. Example: `https://your-minio.com:9000` |
| `PGDATABASE` | The PostgreSQL database |
| `PGHOST` | The PostgreSQL database host to connect to. Example: `yourapp-postgresql` |
| `PGPASSWORD` | The PostgreSQL database password |
| `PGUSER` | The PostgreSQL user account |
