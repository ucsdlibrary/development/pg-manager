#!/usr/bin/env sh
set -e

# This script performs 2 key functions:
# 1. Download a backup/dump created via pg_dump using aws-cli/minio
# 2. Use pg_restore to load that backup file into a postgresql database container

aws --endpoint-url "$ENDPOINT_URL" s3 cp "s3://$BUCKET/$BUCKET_KEY" "$BACKUP_FILE"

# Ensure we can interact with the database
while ! nc -z "$PGHOST" 5432
do
  echo "waiting for postgresql"
  sleep 1
done

pg_restore --verbose -U "$PGUSER" -d "$PGDATABASE" "$BACKUP_FILE"

