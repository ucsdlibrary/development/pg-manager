#!/usr/bin/env sh
set -e

# This script performs 2 key functions:
# 1. Use pg_dump to create a compressed backup file from a postgresql database
# 2. Upload the backup/dump created via pg_dump using aws-cli/minio

# Ensure we can interact with the database
while ! nc -z "$PGHOST" 5432
do
  echo "waiting for postgresql"
  sleep 1
done

pg_dump -Fc -f "$BACKUP_FILE"

aws --endpoint-url "$ENDPOINT_URL" s3 cp "$BACKUP_FILE" "s3://$BUCKET/$BUCKET_KEY"
