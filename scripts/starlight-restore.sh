#!/usr/bin/env sh
set -e

# This script performs 2 key functions:
# 1. Download and restore a backup/dump created via pg_dump using aws-cli/minio.
# 2. Sync/Restore a copy of the starlight public/images directory using aws-cli/minio.

echo "Restoring Starlight database..."
aws --endpoint-url "$ENDPOINT_URL" s3 cp "s3://$BUCKET/$DB_BUCKET_KEY" "$DB_BACKUP_FILE"

# Ensure we can interact with the database
while ! nc -z "$PGHOST" 5432
do
  echo "waiting for postgresql"
  sleep 1
done

pg_restore --single-transaction --clean -U "$PGUSER" -d "$PGDATABASE" "$DB_BACKUP_FILE"

echo "Restoring Starlight public/images directory..."
aws --endpoint-url "$ENDPOINT_URL" s3 sync "s3://$BUCKET/$IMAGES_BUCKET_PATH" /uploads
