FROM alpine:3.13 as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  postgresql-client \
  aws-cli \
  less \
  vim \
  && rm -rf /var/cache/apk/*

RUN mkdir /scripts
COPY scripts/ /scripts
